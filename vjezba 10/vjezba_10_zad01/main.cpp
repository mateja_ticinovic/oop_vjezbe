#include <iostream>
#include <algorithm>
#include <cmath>
#include <fstream>
#include <vector>

using namespace std;

class Error
{
    public:
        string Error_message;
        virtual void Print_error_message()=0;

        void WriteToFile(string errMsg)
        {
            ofstream myFile;
            myFile.open("error.log", ios_base::out | ios_base::app);
            myFile << errMsg;
            myFile.close();
        }

};

class Number_error : public Error
{
    public:
        Number_error()
        {
            Print_error_message();
        }

    private:
        void Print_error_message()
        {
            Error_message = "Your input is not correct\n" ;
            cout << Error_message;
            WriteToFile(Error_message);
        }

};

class Operator_error : public Error
{
    public:
        Operation_error()
        {
            Print_error_message();
        }

    private:
        void Print_error_message()
        {
            Error_message = "You didn't choose operator correctly!\n" ;
            cout << Error_message;
            WriteToFile(Error_message);
        }

};

class Operation_error : public Error
{
    public:
        Operation_error()
        {
            Print_error_message();
        }

    private:
        void Print_error_message()
        {
            Error_message = "Operation has not succeded!! Maybe you were divided with zero so check it out!\n";
            cout << Error_message;
            WriteToFile(Error_message);
        }
};

int number()
{
    int num;
    cout << "Enter number: ";
    cin >> num;

    if(cin.fail())
    {
        throw Number_error();
    }
    else
    {
         return num;
    }

}

char selecting_operation()
{
    char selected_operator;
    cout << "Enter operation you want (+, -, *, /)";
    cin >> selected_operator;

    if(selected_operator!='+' && selected_operator!='-' && selected_operator!='*' && selected_operator!='/')
        throw Operator_error();
    else
        return selected_operator;
}

int operation_result(int a, int b, char selected_operator)
{
    switch(selected_operator)
    {

    case '+':
        return a+b;

    case '-':
        return a-b;

    case '*':
        return a*b;

    case '/':
        if(b==0)
            throw Operation_error();
        else
            return a/b;
        break;

    default:
        throw Operation_error();
    }
}

int main()
{
    int a, b, result;
    char operation;
    vector<int> Results;

    while(true)
    {
        a = number();
        b = number();
        operation = selecting_operation();
        result = operation_result(a,b,operation);

        cout << a << " " << operation << " " << b << " = " << result << endl;
        Results.push_back(result);

        try{a = number();}
        catch(Number_error e){return 0;}

        try{operation = selecting_operation();}
        catch(Operator_error e){return 0;}

        try{b = number();}
        catch(Number_error e){return 0;}

        try{
            result = operation_result(a, b, operation);
            Results.push_back(result);
        }
        catch(Operation_error e){return 0;}

    }

}
