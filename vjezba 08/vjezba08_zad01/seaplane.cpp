#include "seaplane.h"

using namespace OSS;

std::string Seaplane::vehicle_type()
{
    return "Water-air vehicle";
}

unsigned Seaplane::passengers_number()
{
    return passengers;
}
