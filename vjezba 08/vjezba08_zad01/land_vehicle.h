#ifndef LAND_VEHICLE_H
#define LAND_VEHICLE_H
#include <string>
#include "vehicle.h"

namespace OSS
{
    class Land_vehicle : virtual public Vehicle
    {
         protected:
            std::string type;
            unsigned passengers;

        public:
            Land_vehicle():type("land vehicle"){};
            std::string vehicle_type();

    };
}

#endif // LAND_VEHICLE_H
