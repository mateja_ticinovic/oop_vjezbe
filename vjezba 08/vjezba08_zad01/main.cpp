#include <iostream>
#include "vehicle.h"
#include "land_vehicle.h"
#include "watercraft.h"
#include "aircraft.h"
#include "bike.h"
#include "car.h"
#include "catamaran.h"
#include "ferry.h"
#include "seaplane.h"
#include "counter.h"

using namespace OSS;

int main(void){

    Counter c;
    Vehicle* travel[] = {new Bike, new Car, new Catamaran(30), new Ferry(5,5,4),new Seaplane(15)};
    size_t sz = sizeof travel/sizeof travel[0];
    for (unsigned i = 0; i < sz; ++i)
       {
           c.count_passengers(travel[i]);
       }
    std::cout << c.sum << " people are traveling. \n";
    /*for (unsigned i = 0; i < sz; ++i)
        delete travel[i];*/
}
