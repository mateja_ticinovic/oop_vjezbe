#ifndef SEAPLANE_H
#define SEAPLANE_H
#include "watercraft.h"
#include "aircraft.h"

namespace OSS
{
    class Seaplane : public Watercraft, public Aircraft
    {
        protected:
            std::string type;
            unsigned passengers;

        public:
            Seaplane(unsigned max_passengers) : passengers(max_passengers){};
            std::string vehicle_type();
            unsigned passengers_number();


    };
}


#endif // SEAPLANE_H
