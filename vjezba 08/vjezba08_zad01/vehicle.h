#ifndef VEHICLE_H
#define VEHICLE_H
#include <string>

namespace OSS
{
    class Vehicle
    {
        public:
            ~Vehicle();

            virtual std::string vehicle_type()=0;
            virtual unsigned passengers_number()=0;

    };

}

#endif // VEHICLE_H
