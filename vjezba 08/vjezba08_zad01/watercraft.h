#ifndef WATERCRAFT_H
#define WATERCRAFT_H
#include <string>
#include "vehicle.h"

namespace OSS
{
    class Watercraft : virtual public Vehicle
    {
        protected:
            std::string type;
            unsigned passengers;

        public:
            Watercraft():type("water vehicle"){};
            std::string vehicle_type();

    };
}


#endif // WATERCRAFT_H
