#ifndef CAR_H
#define CAR_H
#include "land_vehicle.h"

namespace OSS
{
    class Car : public Land_vehicle
    {
        protected:
            unsigned passengers;

        public:
            Car():passengers(5){};
            unsigned passengers_number();
    };
}


#endif // CAR_H
