#ifndef BIKE_H
#define BIKE_H
#include "land_vehicle.h"

namespace OSS
{
    class Bike : public Land_vehicle
    {
        protected:
            unsigned passengers;

        public:
            Bike():passengers(1){};
            unsigned passengers_number();


    };
}


#endif // BIKE_H
