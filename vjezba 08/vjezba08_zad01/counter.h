#ifndef COUNTER_H
#define COUNTER_H
#include "vehicle.h"

namespace OSS
{
    class Counter
    {
        public:
            Counter(): sum(0) {};
            unsigned sum;

            void count_passengers(Vehicle* v);
    };
}

#endif // COUNTER_H
