#ifndef FERRY_H
#define FERRY_H
#include "watercraft.h"

namespace OSS
{
    class Ferry : public Watercraft
    {
        protected:
            unsigned passengers;

        public:
            Ferry(unsigned passengers, unsigned bikes, unsigned cars):passengers(passengers+bikes+(cars*5)){};
            unsigned passengers_number();


    };
}


#endif // FERRY_H
