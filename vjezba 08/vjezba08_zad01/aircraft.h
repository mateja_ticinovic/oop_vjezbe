#ifndef AIRCRAFT_H
#define AIRCRAFT_H
#include <string>
#include "vehicle.h"

namespace OSS
{
    class Aircraft : virtual public Vehicle
    {
        protected:
            std::string type;
            unsigned passengers;

        public:
            Aircraft():type("air vehicle"){};
            std::string vehicle_type();


    };
}

#endif // AIRCRAFT_H
