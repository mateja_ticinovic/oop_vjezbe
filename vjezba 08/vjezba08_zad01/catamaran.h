#ifndef CATAMARAN_H
#define CATAMARAN_H
#include "watercraft.h"

namespace OSS
{
    class Catamaran : public Watercraft
    {
        protected:
            unsigned passengers;

        public:
            Catamaran(unsigned passengers_num):passengers(passengers_num){};
            unsigned passengers_number();

    };
}

#endif // CATAMARAN_H
