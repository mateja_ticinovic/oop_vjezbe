#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include "function.h"

using namespace std;

int main()
{
    vector<int> vec_of_numbs;
    vector<int> sec_vec_of_numbs;
    vector<int> form_vec;

    srand (time(NULL));
    int a = 0, b = 100, n = 5;
    bool adding_or_generating;
    vec_of_numbs = vector_input(true);
    sec_vec_of_numbs = vector_input(false);
    vector_display(vec_of_numbs);
    vector_display(sec_vec_of_numbs);

    form_vec = form_new_vec(vec_of_numbs, sec_vec_of_numbs,n);
    vector_display(form_vec);
}
