#ifndef FUNCTION_H_INCLUDED
#define FUNCTION_H_INCLUDED
#include <vector>

using namespace std;


vector<int> vector_input(bool adding_or_generating, int a=0, int b=100, int n=5);

void vector_display(vector<int> vec_of_numbs);

vector<int> form_new_vec(vector<int> vec_of_numbs, vector<int> sec_vec_of_numbs, int n=5);

#endif // FUNCTION_H_INCLUDED
