#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <algorithm>

using namespace std;

vector<int> vector_input(bool adding_or_generating, int a=0, int b=100, int n=5 )
{
    vector<int> vec_of_numbs;

    if(adding_or_generating)
    {
        for(int i = 0; i < n; i++)
        {
            int number = rand() % 100;

            if(number > a && number < b)
                vec_of_numbs.push_back(number);
        }

    }
    else
    {
        for(int i = 0; i < n; i++)
        {
            int number = 0;
            cout << "Enter " << i+1 << ". number for your vector (numbers 1-100): " << endl;
            cin >> number;

            while(!(number > a && number < b))
            {
                cout << "Error! Enter number for your vector (numbers 1-100): " << endl;
                cin >> number;

            }
            vec_of_numbs.push_back(number);
        }
    }
    return vec_of_numbs;
    cout << "\n";
}

void vector_display(vector<int> vec_of_numbs)
{
    cout << "Your vector contains: ";
    for(int i = 0; i < vec_of_numbs.size(); i++)
    {
        cout << vec_of_numbs[i] << " ";
    }
    cout << "\n";
}

vector<int> form_new_vec(vector<int> vec_of_numbs, vector<int> sec_vec_of_numbs, int n=5)
{
    vector<int> result;

    for(int i = 0; i < n; i++)
    {
        if(!binary_search(sec_vec_of_numbs.begin(), sec_vec_of_numbs.end(), vec_of_numbs[i]))
            result.push_back(vec_of_numbs[i]);
        else
            continue;
    }

    return result;
}
