#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

using namespace std;

vector<int> vector_input(bool adding_or_generating, int a=0, int b=100, int n=5 )
{
    vector<int> vec_of_numbs;

    if(adding_or_generating)
    {
        for(int i = 0; i < n; i++)
        {
            int number = rand() % 100;

            if(number > a && number < b)
                vec_of_numbs.push_back(number);
        }

    }
    else
    {
        for(int i = 0; i < n; i++)
        {
            int number = 0;
            cout << "Enter " << i+1 << ". number for your vector (numbers 1-100): " << endl;
            cin >> number;

            while(!(number > a && number < b))
            {
                cout << "Error! Enter number for your vector (numbers 1-100): " << endl;
                cin >> number;

            }
            vec_of_numbs.push_back(number);
        }
    }
    return vec_of_numbs;
}
void vector_display(vector<int> vec_of_numbs)
{
    cout << "Your vector contains: ";
    for(int i = 0; i < vec_of_numbs.size(); i++)
    {
        cout << vec_of_numbs[i] << " ";
    }
}
int main()
{
    vector<int> vec_of_numbs;

    srand (time(NULL));
    int a = 0, b = 100, n = 5;
    bool adding_or_generating = false;

    vec_of_numbs = vector_input(adding_or_generating);
    vector_display(vec_of_numbs);
}
