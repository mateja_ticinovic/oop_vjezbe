#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Producent
{
	string name, movie;
	int year;
};

struct Statistics
{
	string name;
	int occurances;
};

vector<Producent> adding_to_list(vector<Producent> v, int prod_list_size)
{

    vector<Producent> temp;

    for(int i = 0; i < prod_list_size; i++)
    {
        Producent p;

        cout << "Enter producent name: ";
        cin.ignore();
        getline(cin, p.name);

        cout << "Enter movie name: ";
        getline(cin, p.movie);

        cout << "Enter year of film's release: ";
        cin >> p.year;

        temp.push_back(p);

        cout << "Added to vector" << "\n" << "************************************" << endl;
    }

    return temp;
}

void get_most_common(vector <Producent> director_list)
{
	vector <Statistics> mostCommon;
	int max = 0, br=0;
	bool flag = false;

	for (unsigned int i = 0; i < director_list.size(); i++)
    {
		br = 1;
		flag = false;

		for (unsigned int x = 0; x < mostCommon.size(); x++)
        {
			if (director_list[i].name == mostCommon[x].name)
			{
				mostCommon[x].occurances++;
				flag = true;
			}
		}
		if (flag == false)
		{
			Statistics director;
			director.name = director_list[i].name;
			director.occurances = 1;
			mostCommon.push_back(director);
		}

		for (unsigned int j = i+1; j < director_list.size(); j++)
        {
			if (director_list[i].name == director_list[j].name)
				br++;
		}

		if (br > max)
        {
			max = br;
		}
	}

	for (unsigned int i = 0; i < mostCommon.size(); i++)
    {
		if (mostCommon[i].occurances < max)
			mostCommon.erase(mostCommon.begin() + i);
	}


	for (unsigned int i = 0; i < mostCommon.size(); i++)
    {
		cout << "Searched director is: " << mostCommon[i].name << endl;
	}
}

int main()
{
    vector<Producent> director_list;
    int prod_list_size= 5;

    director_list = adding_to_list(director_list, prod_list_size);

    for(int i=0; i < director_list.size(); i++)
    {
        cout << director_list[i].name << " " << director_list[i].movie << " " << director_list[i].year << endl;
    }

    get_most_common(director_list);
}
