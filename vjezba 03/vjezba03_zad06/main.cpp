#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

void enter_strings(vector<string> &strings)
{
	int N;

	cout << "Unesite broj stringova: ";
	cin >> N;

	cout << endl << "Enter " << N << " strings:" << endl;
	for (int i = 0; i < N; i++)
    {
		string line;
		cin >> line;

		if (line.length() < 20)
        {
			bool flag = false;

			for (unsigned int i = 0; i < line.length(); i++)
            {

				if (!(isalpha(line[i]) && isupper(line[i])) && !isdigit(line[i]))
					flag = true;
			}

			if (flag == false)
				strings.push_back(line);

			else
            {
				cout << "The string contains symbols that are not upper case or alphanumeric! Enter again!" << endl;
				i--;
			}


		}
		else
        {
			cout << "The string is too long, it needs to be under 20 characters! Enter again!" << endl;;
			i--;
		}

	}
}

void transform(vector<string>& strings)
{
	int count = 2;
	string temp;

	//Setanje kroz vektor stringova
	for (int x = 0; x < strings.size(); x++)
    {

		//Setanje po slovima pojedinog stringa
		for (int i = 0; i < strings[x].length(); i++)
        {
			count = 0;

			//Provjera je li iduce slovo u stringu jednako trenutnom
			int next = i + 1;
			if (strings[x][i] == strings[x][next])
				count = 1;
			else if(!isdigit(strings[x][i]))	//Ako nije slovo onda spremanje iduceg znaka jer je sam (ako nije broj)
				temp += strings[x][i];

			//Prebrojavanje svih iducih istih slova
			while (strings[x][i] == strings[x][next])
            {
				next++;
				count++;
			}

			//Spremanje u temp string i namjestanje int i na mjesto nakon svih istih slova
			if (count > 1)
            {
                char buffer[2];
                itoa(count, buffer, 10);
                cout << buffer << endl;
				temp += buffer;
				temp += strings[x][i];
				i += count - 1;
			}

			//Spremanje za slucaj da je broj ispred slova
			if (isdigit(strings[x][i]))
            {
				next = i + 1;

				if (isalpha(strings[x][next]))
                {
					int times = strings[x][i] - '0';

					//Spremanje slova onoliko puta koliki je broj ispred tog slova
					for (int j = 0; j < times; j++)
						temp += strings[x][next];

					i++;
				}
			}
		}

		strings[x] = temp;
		temp.clear();

	}
}

void print_strings(vector<string> strings)
{

	cout << endl << "Ispis stringova:" << endl;

	for (int i = 0; i < strings.size(); i++)
		cout << strings[i] << endl;
}

int main()
{

	vector <string> strings;
	enter_strings(strings);

	transform(strings);

	print_strings(strings);
}
