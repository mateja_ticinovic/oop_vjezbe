#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <time.h>
#include <cctype>

using namespace std;

int main()
{
    vector<int> matches_left;

    bool computer_turn = true, player_turn = false;
    int total_matches = 21;

    string player1;

    cout << "********** WELCOME TO PICK A MATCH GAME **********" << endl;
    cout << "\n";

    cout << "\n-------------------- RULES: --------------------\n";
    cout <<"\n1.) You can take 1,2 or 3 matches in a turn.\n2.) Player who takes last one loses.\n3.) You'll be playing against computer.";
    cout <<"\n4.) You choose number of matches you want to take by yourself.\n5.) For computer it is random generated.\n" << endl;

    cout << "\n----------------- @pick_a_match_game@ ----------------\n" << endl;

    cout << "Before starting a game, tell us your name. \nPlease, enter your name here: ";
    cin >> player1;


    srand (time(NULL));

    matches_left.push_back(total_matches);

    cout << "\n\n***** NOW, THE GAME IS STARTING *****\n" << endl;

    int computer_picked_up = 0, player_picked_up = 0;

    while(total_matches != 1)
    {
        if(computer_turn == true)
            {

                if(total_matches == 1)
                {
                    cout << player1 << " won!!" << endl;
                    cout << "\n";
                    break;
                }

                else if(total_matches == 2)
                {
                    computer_picked_up = 1;
                    matches_left.push_back(1);
                    cout << "Computer took" << " " << computer_picked_up << " match(es)" << endl;
                    cout << "Computer won!!!" << endl;
                    cout << "\n";
                    break;
                }

                else if(total_matches == 3)
                {
                    computer_picked_up = 2;
                    matches_left.push_back(2);
                    cout << "Computer took" << " " << computer_picked_up << " match(es)" << endl;
                    cout << "Computer won!!!" << endl;
                    cout << "\n";
                    break;
                }

                else
                {
                    cout << "----> COMPUTER'S TURN <----" << endl;
                    computer_picked_up = rand() % 3 + 1;

                    cout << "Computer took" << " " << computer_picked_up << " match(es)" << endl;

                    total_matches = total_matches - computer_picked_up;
                    matches_left.push_back(total_matches);

                    cout << "Number of matches left: " << total_matches << endl;
                    cout << "--------------------------------------" << endl;
                }

                computer_turn = false;
                player_turn = true;
            }


        else if(player_turn == true)
            {
                cout << "\n----> " << player1 << "'S TURN <----" << endl;

                 if(total_matches == 1)
                {
                    cout << "Computer won!!" << endl;
                    cout << "\n";
                    break;
                }

                else if(total_matches == 2)
                {
                    player_picked_up = 1;
                    matches_left.push_back(1);
                    cout << player1 << " took" << " " << player_picked_up << " match(es)" << endl;
                    cout << player1 << " won!!!" << endl;
                    cout << "\n";
                    break;
                }

                else if(total_matches == 3)
                {
                    player_picked_up = 2;
                    matches_left.push_back(2);
                    cout << player1 << " took" << " " << player_picked_up << " match(es)" << endl;
                    cout << player1 << " won!!!" << endl;
                    cout << "\n";
                    break;
                }

                else
                {
                    cout << "Enter how many matches you " << player1 << " want to take (1-3): ";
                    cin >> player_picked_up;


                    while (player_picked_up < 1 || player_picked_up > 3)
                    {
                        cout << "You can't take " << player_picked_up << " matches." << endl;
                        cout << "Please enter correct value (1-3): ";
                        cin >> player_picked_up;
                    }
                    cout << player1 << " took " << player_picked_up << " match(es)." << endl;

                    total_matches = total_matches - player_picked_up;
                    matches_left.push_back(total_matches);
                    cout << "Number of matches left: " << total_matches << endl;
                    cout << "--------------------------------------" << endl;
                    cout << "\n";
                }
                player_turn = false;
                computer_turn = true;
            }
    }

    cout << "This is how was going number of total matches during game: " << endl;
    for(int i = 0; i < matches_left.size(); i++)
    {
    cout << matches_left[i] << " ";
    }
}

