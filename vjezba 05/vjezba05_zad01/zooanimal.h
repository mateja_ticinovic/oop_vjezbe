#ifndef ZOOANIMAL_H
#define ZOOANIMAL_H
#include <string>
#include <vector>

using namespace std;

class weight_throw_years
{
    public:
        int year;
        double weight;
};

class ZooAnimal
{
    private:
        string name, specie;
        int birth_year, cage_number, daily_meal_number, life_duration, numb = 0;
    public:
        ZooAnimal(string name, string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration);
        ZooAnimal(const ZooAnimal& animal);
        ~ZooAnimal();


        void display_data();
        void enter_weigth_data(int year, int weight);
        void changing_daily_meal_number(int a);
        int detecting_weight();

        weight_throw_years* tracking_weight;
        weight_throw_years temp_weight;
        vector<weight_throw_years> weights;

};

#endif // ZOOANIMAL_H
