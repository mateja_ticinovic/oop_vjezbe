#include <iostream>
#include <vector>
#include "zooanimal.h"

using namespace std;

int main()
{
    vector<ZooAnimal> animals;

    ZooAnimal lav ("lav", "macka", 1793, 7, 7, 35);
    ZooAnimal macka ("macka", "macka", 2007, 3, 5, 2);
    ZooAnimal sokol ("sokol", "ptica", 2000, 13, 2, 27);

    lav.enter_weigth_data(2019, 100);
    macka.enter_weigth_data(2019,10);
    sokol.enter_weigth_data(2019, 51);

    animals.push_back(lav);
    animals.push_back(macka);
    animals.push_back(sokol);



    for (vector<ZooAnimal>::iterator it = animals.begin(); it != animals.end(); it++)
    {
        it->display_data();
    }

    lav.changing_daily_meal_number(lav.detecting_weight());
    macka.changing_daily_meal_number(macka.detecting_weight());
    sokol.changing_daily_meal_number(sokol.detecting_weight());
}
