#include <iostream>
#include "vector.h"

using namespace vec;

Vec3::Vec3()
{
    x = 0;
    y = 0;
    z = 0;
}

Vec3::Vec3(int x1, int y1, int z1)
{
    x = x1;
    y = y1;
    z = z1;
}

Vec3::Vec3(const Vec3 &v)
{
    x = v.x;
    y = v.y;
    z = v.z;
}

Vec3& Vec3::operator=(const Vec3& v)
{
    x = v.x;
    y = v.y;
    z = v.z;

    return *this;
}

Vec3 Vec3::operator+(const Vec3 &v)
{
    return Vec3(x+v.x, y+v.y, z+v.z);
}

Vec3& Vec3::operator+=(const Vec3 &v)
{
    x += v.x;
    y += v.y;
    z += v.z;

    return *this;
}

Vec3 Vec3::operator-(const Vec3 &v)
{
    return Vec3(x-v.x, y-v.y, z-v.z);
}

Vec3& Vec3::operator-=(const Vec3 &v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;

    return *this;
}

Vec3 Vec3::operator*(int scalar)
{
    return Vec3(x*scalar, y*scalar, z*scalar);
}

Vec3& Vec3::operator*=(int scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;

    return *this;
}

Vec3 Vec3::operator/(int scalar)
{
    return Vec3(x/scalar, y/scalar, z/scalar);
}

Vec3& Vec3::operator/=(int scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;

    return *this;
}

bool Vec3::operator==(const Vec3 &v)
{
    return((x == v.x) && (y == v.y) && (z == v.z));
}

bool Vec3::operator!=(const Vec3 &v)
{
    return((x != v.x) || (y != v.y) || (z != v.z));
}

int Vec3::operator[](int i)
{
    if (i == 0)
    {
        cout << "Vector's first coordinate is: ";
        return x;
    }

    else if (i == 1)
    {
        cout << "Vector's second coordinate is: ";
        return y;
    }

    else if (i == 2)
    {
        cout << "Vector's third coordinate is: ";
        return z;
    }

    else
    {
        cout << "[] Access error!" << endl;
        return 0;
    }
}
