#ifndef VECTOR_H
#define VECTOR_H
#include <iostream>
#include <math.h>

using namespace std;

namespace vec
{
    class Vec3
    {
        private:
            int x, y, z;

        public:
            static int vecCount;

            Vec3();
            Vec3(int x1, int y1, int z1);
            Vec3 (const Vec3 &v);

            friend std::istream& operator>>(std::istream& input,  Vec3& v){
                cout << "------------------------------" << endl;

                cout << "Enter vector's x coordinate: ";
                input >> v.x;

                cout << "Enter vector's y coordinate: ";
                input >> v.y;

                cout << "Enter vector's z coordinate: ";
                input >> v.z;

                cout << "------------------------------" << endl;

                return input;};

            friend std::ostream& operator<<(std::ostream& output , const Vec3& v){
                output << '(' << v.x << ',' << v.y << ','<< v.z << ')' << endl;

                cout << "------------------------------" << endl;
                return output ;};

            Vec3& operator=(const Vec3& v);

            Vec3 operator+(const Vec3 &v);
            Vec3& operator+=(const Vec3 &v);

            Vec3 operator-(const Vec3 &v);
            Vec3& operator-=(const Vec3 &v);

            Vec3 operator*(int scalar);
            Vec3& operator*=(int scalar);

            Vec3 operator/(int scalar);
            Vec3& operator/=(int scalar);

            bool operator==(const Vec3 &v);
            bool operator!=(const Vec3 &v);

            int operator[](int i);
    };

}
#endif
