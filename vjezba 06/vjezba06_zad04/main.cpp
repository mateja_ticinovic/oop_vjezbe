#include <iostream>
#include "vector.h"

using namespace std;

int main()
{
    vec::Vec3 v, v1, v2;
    //bool vec::Vec3::equal1_2;

    cout << "INPUTING VECTOR'S COORDINATES: (vector v) " << endl;
    cin >> v;

    cout << "OUTPUTING VECTOR'S COORDINATES: (vector v)" << endl;
    cout << v;

    v1 = v;
    cout << "DISPLAYING VECTOR'S COORDINATES: (vector v1)" << endl;
    cout << v1;

    cout << "INPUTING VECTOR'S COORDINATES: (vector v2)" << endl;
    cin >> v2;

    cout << "OUTPUTING VECTOR'S COORDINATES: (vector v2)" << endl;
    cout << v2;

    vec::Vec3 sum = v1 + v2;
    cout << "RESULT OF ADDING: (v1 + v2)" << endl;
    cout << sum;

    vec::Vec3 substraction = v1 - v2;
    cout << "RESULT OF SUBSTRACTION: (v1 - v2)" << endl;
    cout << substraction;

    vec::Vec3 multiplication_result = v1 * 2;
    cout << "MULTIPLICATON RESULT (v1 *2):" << endl;
    cout << multiplication_result;

    vec::Vec3 division_result = v2 /2;
    cout << "DIVISION RESULT (v2/2):" << endl;
    cout << division_result;

    //bool jednako = (v1==v2);
    cout << "COMPARING EQUALITY (v1 i v2):" << endl;
    cout << (v1 == v2) << endl;


    cout << "COMPARING NOT EQUALITY (v1 i v2)" << endl;
    cout << (v1 != v2) << endl;

    int value = v1[1];
    cout << value << endl;

}
