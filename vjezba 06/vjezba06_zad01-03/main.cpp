#include <iostream>
#include "vector3.h"

using namespace std;

int main()
{
    Vec3 v, v1, v2;

    cout << "INPUTING VECTOR'S COORDINATES: (vector v) " << endl;
    cin >> v;

    cout << "OUTPUTING VECTOR'S COORDINATES: (vector v)" << endl;
    cout << v;

    v1 = v;
    cout << "DISPLAYING VECTOR'S COORDINATES: (vector v1)" << endl;
    cout << v1;

    cout << "INPUTING VECTOR'S COORDINATES: (vector v2)" << endl;
    cin >> v2;

    cout << "OUTPUTING VECTOR'S COORDINATES: (vector v2)" << endl;
    cout << v2;

    Vec3 sum = v1 + v2;
    cout << "RESULT OF ADDING: (v1 + v2)" << endl;
    cout << sum;

    Vec3 substraction = v1 - v2;
    cout << "RESULT OF SUBSTRACTION: (v1 - v2)" << endl;
    cout << substraction;

    Vec3 multiplication_result = v1 * 2;
    cout << "MULTIPLICATON RESULT (v1 *2):" << endl;
    cout << multiplication_result;

    Vec3 division_result = v2 /2;
    cout << "DIVISION RESULT (v2/2):" << endl;
    cout << division_result;

    bool equal1_2 = (v1 == v2);
    cout << "COMPARING EQUALITY (v1 i v2):" << endl;
    cout << equal1_2 << endl;


   bool not_equal1_2 = (v1 != v2);
   cout << "COMPARING NOT EQUALITY (v1 i v2)" << endl;
   cout << not_equal1_2 << endl;

   int value = v1[1];
   cout << value << endl;

   Vec3 normalized_vec = normalized_vec.normalizing_vector(v1);
   cout << normalized_vec << endl;

    cout << "Total vector number: " << Vec3::vecCount << endl;
}
