#ifndef VECTOR3_H
#define VECTOR3_H
#include <iostream>

using namespace std;

class Vec3
{
    private:
        int x, y, z;

    public:
        static int vecCount;
        Vec3();
        Vec3(int x1, int y1, int z1);
        Vec3 (const Vec3 &v);

        friend istream& operator>>(istream&, Vec3&);
        friend ostream& operator<<(ostream& os , const Vec3& v);
        Vec3& operator=(const Vec3& v);
        Vec3 operator+(const Vec3 &v);
        Vec3& operator+=(const Vec3 &v);
        Vec3 operator-(const Vec3 &v);
        Vec3& operator-=(const Vec3 &v);
        Vec3 operator*(int scalar);
        Vec3& operator*=(int scalar);
        Vec3 operator/(int scalar);
        Vec3& operator/=(int scalar);
        bool operator==(const Vec3 &v);
        bool operator!=(const Vec3 &v);
        int operator[](int i);

        Vec3 normalizing_vector(Vec3 &v);


};


#endif // VECTOR3_H
