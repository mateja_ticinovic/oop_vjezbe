#include "vector3.h"
#include "math.h"

using namespace std;

int Vec3::vecCount = 0;

Vec3::Vec3() //default konstruktor
{
    x = 0;
    y = 0;
    z = 0;

    vecCount++;
}

Vec3::Vec3(int x1, int y1, int z1) //konstruktor s argumentima
{
    x = x1;
    y = y1;
    z = z1;

    vecCount++;

}

Vec3::Vec3(const Vec3 &v) //copy konstruktor
{
    x = v.x;
    y = v.y;
    z = v.z;

    vecCount++;

}

istream& operator>>(istream& input , Vec3& v) //overloading input
{
    cout << "------------------------------" << endl;

    cout << "Enter vector's x coordinate: ";
    input >> v.x;

    cout << "Enter vector's y coordinate: ";
    input >> v.y;

    cout << "Enter vector's z coordinate: ";
    input >> v.z;

    cout << "------------------------------" << endl;

    return input;
}

ostream& operator<<(ostream& output , const Vec3& v ) //overloading output
{
    output << '(' << v.x << ',' << v.y << ','<< v.z << ')' << endl;

    cout << "------------------------------" << endl;
    return output ;
}

Vec3& Vec3::operator=(const Vec3& v)
{
    x = v.x;
    y = v.y;
    z = v.z;

    return *this;
}

Vec3 Vec3::operator+(const Vec3 &v)
{
    return Vec3(x+v.x, y+v.y, z+v.z);
}

Vec3& Vec3::operator+=(const Vec3 &v)
{
    x += v.x;
    y += v.y;
    z += v.z;

    return *this;
}

Vec3 Vec3::operator-(const Vec3 &v)
{
    return Vec3(x-v.x, y-v.y, z-v.z);
}

Vec3& Vec3::operator-=(const Vec3 &v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;

    return *this;
}

Vec3 Vec3::operator*(int scalar)
{
    return Vec3(x*scalar, y*scalar, z*scalar);
}

Vec3& Vec3::operator*=(int scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;

    return *this;
}

Vec3 Vec3::operator/(int scalar)
{
    return Vec3(x/scalar, y/scalar, z/scalar);
}


Vec3& Vec3::operator/=(int scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;

    return *this;
}

bool Vec3::operator==(const Vec3 &v)
{
    return((x == v.x) && (y == v.y) && (z == v.z));
}

bool Vec3::operator!=(const Vec3 &v)
{
   return((x != v.x) || (y != v.y) || (z != v.z));
}

int Vec3::operator[](int i)
{
    if (i == 0)
    {
        return x;
    }
    else if (i == 1)
    {
        return y;
    }
    else if (i == 2)
    {
        return z;
    }
    else
    {
        cout << "[] Access error!" << endl;
    }
}

Vec3 Vec3::normalizing_vector(Vec3 &v)
{
    float length = (sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z)));

    return Vec3(x/length, y/length, z/length);
}

