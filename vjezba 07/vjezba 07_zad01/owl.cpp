#include "owl.h"

using namespace OSS;

Owl::Owl(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
double average_temperature, int food_quantity) : Bird(name, specie, birth_year, cage_number, daily_meal_number,life_duration, incubation_time,
average_temperature)
{
    this->food_quantity = food_quantity;
}

int Owl::get_daily_meal_quantity()
{
    return daily_meal_number*food_quantity;
}
