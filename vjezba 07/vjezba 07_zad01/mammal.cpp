#include "mammal.h"

using namespace OSS;

Mammal::Mammal(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int gest_period,
double corp_temprature): ZooAnimal(name, specie, birth_year, cage_number, daily_meal_number, life_duration)
{
    this->gest_period = gest_period;
    this->corp_temprature = corp_temprature;
};

int Mammal::get_period() const
{
    return gest_period;
}

double Mammal::get_temperature() const
{
    return corp_temprature;
}



