#ifndef ELEPHANT_H
#define ELEPHANT_H
#include <iostream>
#include "mammal.h"

namespace OSS
{
    class Elephant : public Mammal
    {
        private:
            int food_quantity;

        public:
            Elephant(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int gest_period,
            double corp_temprature, int food_quantity);

            int get_daily_meal_quantity();

            friend std::ostream& operator<< (std::ostream& i, const Elephant& e){
                i << "NAME: " << e.get_name() <<"\nSPECIE: " << e.get_specie() << "\nWAY OF REPRODUCTION: reproducing by giving birth \n" <<
                "GESTATION PERIOD: " << e.get_period() <<" month(s)\nBODY TEMPERATURE: " << e.get_temperature() << " C\n" << std::endl;};

    };
}



#endif // ELEPHANT_H
