#ifndef TURTLE_H
#define TURTLE_H
#include <iostream>
#include "reptile.h"

namespace OSS
{
    class Turtle : public Reptile
    {
        private:
            int food_quantity;

        public:
            Turtle(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
            double surrounding_temperature, int food_quantity);

            int get_daily_meal_quantity();

            friend std::ostream& operator<< (std::ostream& i, Turtle& t){
                i << "NAME: " << t.get_name() <<"\nSPECIE: " <<  t.get_specie() << "\nWAY OF REPRODUCTION: reproducing by laying egg(s) \n" <<
                "INCUBATION TIME: " << t.get_time() <<" month(s)\nSURROUNDING TEMPERATURE: " << t.get_temperature() << " C\n" << "DAILY MEAL QUANTITY: " <<
                t.get_daily_meal_quantity() << "kg('s) of food" << std::endl;};
    };
}



#endif // TURTLE_H
