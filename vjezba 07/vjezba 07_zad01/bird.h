#ifndef BIRD_H
#define BIRD_H
#include "zooanimal.h"

namespace OSS
{
    class Bird : public ZooAnimal
    {
        private:
            int incubation_time;
            double average_temperature;

        public:
            Bird(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
            double average_temperature);

            int get_time() const;
            double get_temperature() const;

    };
}


#endif // BIRD_H
