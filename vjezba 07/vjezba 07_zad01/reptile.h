#ifndef REPTILE_H
#define REPTILE_H
#include "zooanimal.h"

namespace OSS
{
    class Reptile : public ZooAnimal
    {
        protected:
            int incubation_time;
            double surrounding_temperature;

        public:
            Reptile(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
            double surrounding_temperature);

            int get_time() const;
            double get_temperature() const;
        };
}



#endif // REPTILE_H
