#include <iostream>
#include <vector>
#include "zooanimal.h"
#include "tiger.h"
#include "monkey.h"
#include "elephant.h"
#include "griffon_vulture.h"
#include "owl.h"
#include "crocodile.h"
#include "turtle.h"

using namespace OSS;

int main()
{
    std::vector<ZooAnimal> animals;

    Tiger tiger1 ("tiger Leo", "mammal", 2010, 7, 5, 35, 9, 35.8, 10);
    animals.push_back(tiger1);

    Monkey monkey1 ("monkey Peter", "mammal", 2005, 2, 3, 15, 5, 34.6, 5);
    animals.push_back(monkey1);

    Elephant elephant1("elephant Mario", "mammal", 2000, 1, 10, 15, 8, 38.2, 30);
    animals.push_back(elephant1);

    Griffon_Vulture griffon_vulture1("griffon vulture Faydee", "bird", 2013, 20, 7, 6, 10, 35.6, 4);
    animals.push_back(griffon_vulture1);

    Owl owl1("owl Weisy", "bird", 2017, 6, 4, 8, 6, 39.1, 2);
    animals.push_back(owl1);

    Crocodile crocodile1 ("crocodile Dundee", "reptile", 1999, 9, 9, 16, 10, 30.5, 12);
    animals.push_back(crocodile1);

    Turtle turtle1("turtle Steve", "reptile", 1996, 16, 5, 20, 12, 28.9, 5);
    animals.push_back(turtle1);

    Elephant elephant2("elephant Alex", "mammal", 2004, 5, 5, 25, 9, 37.4, 15);
    animals.push_back(elephant2);

    int mammal_food = tiger1.get_daily_meal_quantity() + elephant1.get_daily_meal_quantity() + elephant2.get_daily_meal_quantity() + monkey1.get_daily_meal_quantity();
    int bird_food = griffon_vulture1.get_daily_meal_quantity() + owl1.get_daily_meal_quantity();
    int reptile_food = crocodile1.get_daily_meal_quantity() + turtle1.get_daily_meal_quantity();

    int sum_of_food_quantity = mammal_food + bird_food + reptile_food;

    std::cout << "DISPLAYING DATA OUTSIDE DISPLAY FUNCTION: " << std::endl;
    std::cout << "******************************************" << std::endl;
    std::cout << tiger1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << monkey1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << elephant1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << griffon_vulture1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << owl1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << crocodile1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << turtle1;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << elephant2;
    std::cout << "------------------------------------------" << std::endl;
    std::cout << "******************************************" << std::endl;

    std::cout << "SUM OF FOOD WHICH ZOO HAS TO PROVIDE EVERY DAY FOR ANIMAL IS: " << sum_of_food_quantity << " KG'S." << std::endl;
}

