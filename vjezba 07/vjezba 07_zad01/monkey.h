#ifndef MONKEY_H
#define MONKEY_H
#include <iostream>
#include "mammal.h"

namespace OSS
{
    class Monkey : public Mammal
    {
        private:
            int food_quantity;

        public:
            Monkey(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int gest_period,
            double corp_temprature, int food_quantity);

            int get_daily_meal_quantity();

            friend std::ostream& operator<< (std::ostream& i, const Monkey& m){
                i << "NAME: " << m.get_name() <<"\nSPECIE: " << m.get_specie() << "\nWAY OF REPRODUCTION: reproducing by giving birth \n" <<
                "GESTATION PERIOD: " << m.get_period() <<" month(s)\nBODY TEMPERATURE: " << m.get_temperature() << " C" << std::endl;};

    };
}


#endif // MONKEY_H
