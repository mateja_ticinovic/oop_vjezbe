#ifndef TIGER_H
#define TIGER_H
#include <iostream>
#include "mammal.h"

namespace OSS
{
    class Tiger : public Mammal
    {
        private:
            int food_quantity;

        public:
            Tiger(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int gest_period,
            double corp_temprature, int food_quantity);

            int get_daily_meal_quantity();

            friend std::ostream& operator<< (std::ostream& i, const Tiger& t){
                i << "NAME: " << t.get_name()<< "\n" <<"SPECIE: " << t.get_specie() << "\n" << "WAY OF REPRODUCTION: reproducing by giving birth \n"
                << "GESTATION PERIOD: " << t.get_period()<< " month(s)\n" << "BODY TEMPERATURE: " << t.get_temperature() << " C \n" << std::endl;};
    };
}



#endif // TIGER_H
