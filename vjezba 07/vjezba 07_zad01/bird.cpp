#include "bird.h"

using namespace OSS;

Bird::Bird(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
double average_temperature) : ZooAnimal(name, specie, birth_year, cage_number, daily_meal_number, life_duration)
{
    this->incubation_time = incubation_time;
    this->average_temperature = average_temperature;
};

int Bird::get_time() const
{
    return incubation_time;
}

double Bird::get_temperature() const
{
    return average_temperature;
}
