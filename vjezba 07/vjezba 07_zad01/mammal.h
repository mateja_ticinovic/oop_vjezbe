#ifndef MAMMAL_H
#define MAMMAL_H
#include "zooanimal.h"

namespace OSS
{
    class Mammal : public ZooAnimal
    {
        protected:
            int gest_period;
            double corp_temprature;

        public:
            Mammal(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int gest_period,
            double corp_temprature);

            int get_period() const;
            double get_temperature() const;
    };
}


#endif // MAMMAL_H
