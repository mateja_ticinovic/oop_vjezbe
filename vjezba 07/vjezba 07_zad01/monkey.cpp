#include "monkey.h"

using namespace OSS;

Monkey::Monkey(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int gest_period,
double corp_temprature, int food_quantity) : Mammal (name, specie, birth_year, cage_number, daily_meal_number, life_duration, gest_period,
corp_temprature)
{
    this->food_quantity = food_quantity;
}

int Monkey::get_daily_meal_quantity()
{
    return daily_meal_number*food_quantity;
}
