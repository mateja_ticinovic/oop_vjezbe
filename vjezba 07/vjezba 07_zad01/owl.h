#ifndef OWL_H
#define OWL_H
#include <iostream>
#include "bird.h"

namespace OSS
{
    class Owl : public Bird
    {
        private:
            int food_quantity;

        public:
            Owl(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
            double average_temperature, int food_quantity);

            int get_daily_meal_quantity();

            friend std::ostream& operator<< (std::ostream& i, Owl& o){
                i << "NAME: " << o.get_name() <<"\nSPECIE: " << o.get_specie() << "\nWAY OF REPRODUCTION: reproducing by laying egg(s) \n" <<
                "INCUBATION TIME: " << o.get_time() <<" month(s)\nAVERAGE TEMPERATURE: " << o.get_temperature() << " C\n" << "DAILY MEAL QUANTITY: " <<
                o.get_daily_meal_quantity() << "kg('s) of foof" << std::endl;};

    };
}


#endif // OWL_H
