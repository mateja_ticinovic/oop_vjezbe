#include "zooanimal.h"
#include <iostream>
#include <ctime>

using namespace OSS;

ZooAnimal::ZooAnimal(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration)
{
    this->name = name;
    this->specie = specie;
    this->birth_year = birth_year;
    this->cage_number = cage_number;
    this->daily_meal_number = daily_meal_number;
    this->life_duration = life_duration;

    tracking_weight = new weight_throw_years[life_duration*2];

    for(int i = 0; i < life_duration*2; i++)
    {
        tracking_weight[i].weight = 0;
        tracking_weight[i].year = 0;
    }
}

ZooAnimal::ZooAnimal(const ZooAnimal& animal)
{
    name =  animal.name;
    specie = animal.specie;
    birth_year = animal.birth_year;
    cage_number = animal.cage_number;
    daily_meal_number = animal.daily_meal_number;
    life_duration = animal.life_duration;

    tracking_weight = new weight_throw_years[life_duration*2];
    std::copy(animal.tracking_weight, animal.tracking_weight + life_duration*2, tracking_weight);
}

ZooAnimal::~ZooAnimal()
{
    delete [] tracking_weight;
    tracking_weight = NULL;
}

void ZooAnimal::display_data()
{
    std::cout << "----------------> ANIMAL DATA <----------------" << std::endl;
    std::cout << "NAME: " << name << std::endl;
    std::cout << "SPECIE: " << specie << std::endl;
    std::cout << "BIRTH YEAR: " << birth_year << std::endl;
    std::cout << "CAGE NUMBER: " << cage_number << std::endl;
    std::cout << "DAILY MEAL NUMBER: " << daily_meal_number << std::endl;
    std::cout << "LIFE DURATION: " << life_duration << std::endl;
    std::cout << "-----------------------------------------------" << std::endl;
}

int ZooAnimal::detecting_weight()
{
    time_t now = time(0);
    tm *local_time = localtime(&now);
    time_t current_Year = 1900 + local_time->tm_year;


    for (int i = 1 ; i < life_duration*2 ; i++)
    {
        if (tracking_weight[i].year == current_Year)
        {
            if(tracking_weight[i].weight > (tracking_weight[i-1].weight + tracking_weight[i-1].weight*0.1))
                return 1;
            else if (tracking_weight[i].weight < (tracking_weight[i-1].weight + tracking_weight[i-1].weight*0.1))
                return -1;
            else
                return 0;
        }
    }

}

void ZooAnimal::enter_weigth_data(int year, int weight)
{
    time_t now = time(0);
    tm *local_time = localtime(&now);
    time_t current_Year = 1900 + local_time->tm_year;

    for (int i = 0; i < (life_duration*2); i++)
    {
        if (year > current_Year)
            {
                std::cout << "It isn't possible enter data!!\n";
                break;
            }
        else if (tracking_weight[i].year == year)
        {
            if (tracking_weight[i].year != current_Year && tracking_weight[i].weight == 0)
                tracking_weight[i].weight = weight;
            else if (tracking_weight[i].year == current_Year )
                tracking_weight[i].weight = weight;
            else
                std::cout << "It isn't possible enter data for chosen period!!\n";
        }
    }
}

void ZooAnimal::changing_daily_meal_number(int a)
{
    if (a < 0)
    {

        daily_meal_number++;
        std::cout << "Daily meal number incresed" << std::endl;
    }
    else if (a == 0)
    {
        std::cout << "Daily meal number isn't changing" << std::endl;
    }
    else
    {
        daily_meal_number--;
        std::cout << "Daily meal numbers is lower" << std::endl;
    }
}

std::string ZooAnimal::get_name() const
{
    return name;
}

std::string ZooAnimal::get_specie() const
{
    return specie;
}

int ZooAnimal::get_daily() const
{
    return daily_meal_number;
}
