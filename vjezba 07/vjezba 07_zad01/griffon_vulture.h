#ifndef GRIFFON_VULTURE_H
#define GRIFFON_VULTURE_H
#include <iostream>
#include "bird.h"

namespace OSS
{
    class Griffon_Vulture : public Bird
    {
        protected:
            int food_quantity;

        public:
            Griffon_Vulture(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
            double average_temperature, int food_quantity);

            int get_daily_meal_quantity()const;

            friend std::ostream& operator<< (std::ostream& i, Griffon_Vulture& gf){
                i << "NAME: " << gf.get_name() << "\nSPECIE: " << gf.get_specie() << "\nWAY OF REPRODUCTION: reproducing by laying egg(s) \n" <<
                "INCUBATION TIME: " << gf.get_time() << " month(s)\nAVERAGE TEMPERATURE: " << gf.get_temperature() << " C\n" <<
                "DAILY MEAL QUANTITY: " << gf.get_daily_meal_quantity() << "kg('s) of food" << std::endl;};
        };
}



#endif // GRIFFON_VULTURE_H
