#ifndef ZOOANIMAL_H
#define ZOOANIMAL_H
#include <string>
#include <vector>

namespace OSS
{
    class weight_throw_years
    {
        public:
            int year;
            double weight;
    };

    class ZooAnimal
    {
        protected:
            std::string name, specie;
            int birth_year, cage_number, daily_meal_number, life_duration, numb = 0;

        public:
            ZooAnimal(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration);
            ZooAnimal(const ZooAnimal& animal);
            ~ZooAnimal();


            void display_data();
            void enter_weigth_data(int year, int weight);
            void changing_daily_meal_number(int a);
            int detecting_weight();
            int count_food_sum();

            weight_throw_years* tracking_weight;

            std::string get_name()const;
            std::string get_specie()const;
            int get_daily() const;

    };
}
#endif // ZOOANIMAL_H


