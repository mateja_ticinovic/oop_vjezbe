#ifndef CROCODILE_H
#define CROCODILE_H
#include <iostream>
#include "reptile.h"

namespace OSS
{
    class Crocodile : public Reptile
    {
        protected:
            int food_quantity;

        public:
            Crocodile(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
            double surrounding_temperature, int food_quantity);


            int get_daily_meal_quantity();

            friend std::ostream& operator<< (std::ostream& i, Crocodile& c){
                i << "NAME: " << c.get_name() <<"\nSPECIE: " << c.get_specie() << "\nWAY OF REPRODUCTION: reproducing by laying egg(s) \n" <<
                "INCUBATION TIME: " << c.get_time() <<" month(s)\nSURROUNDING TEMPERATURE: " << c.get_temperature() << " C\n" << "DAILY MEAL QUANTITY: " <<
                c.get_daily_meal_quantity() << " kg('s) of food" << std::endl;};

    };
}



#endif // CROCODILE_H
