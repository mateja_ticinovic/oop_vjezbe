#include "crocodile.h"

using namespace OSS;

Crocodile::Crocodile(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
double surrounding_temperature, int food_quantity) : Reptile (name, specie, birth_year, cage_number, daily_meal_number, life_duration,
incubation_time, surrounding_temperature)
{
    this->food_quantity = food_quantity;
}

int Crocodile::get_daily_meal_quantity()
{
    return daily_meal_number*food_quantity;
}


