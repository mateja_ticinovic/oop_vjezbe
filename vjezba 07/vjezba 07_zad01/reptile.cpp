#include "reptile.h"

using namespace OSS;

Reptile::Reptile(std::string name, std::string specie, int birth_year, int cage_number, int daily_meal_number, int life_duration, int incubation_time,
double surrounding_temperature) : ZooAnimal(name, specie, birth_year, cage_number, daily_meal_number, life_duration)
{
    this->incubation_time = incubation_time;
    this->surrounding_temperature = surrounding_temperature;
};

int Reptile::get_time() const
 {
     return incubation_time;
 }

 double Reptile::get_temperature() const
 {
     return surrounding_temperature;
 }
