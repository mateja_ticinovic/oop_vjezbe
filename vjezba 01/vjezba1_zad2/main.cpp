#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <string>

using namespace std;

struct student{

    int ID_student;
    string student_name;
    char student_gender;
    int quizz_one;
    int quizz_two;
    int mid_term_score;
    int final_score;
    int year_final_scores;

};

void add_student(student array_of_students[], int size_of_stArray, int& position)
{
    cout << "******** ADDING NEW STUDENT: ********" << endl;

    int number_of_students = 0;
    cout << "\n" << "Please enter how many students You want to add:";
    cin >> number_of_students;

    if(number_of_students > size_of_stArray && number_of_students < position)
        size_of_stArray = number_of_students;

    if (position > 19)
    {
        cout << "Razred je popunjen" << endl;
    }
    else
    {
        for(int i = 0; i < number_of_students; i++)
        {
            cout << "Enter student ID:";
            cin >> array_of_students[position].ID_student;
            cout << "\n";

            cout << "Enter student name:";
            cin >> array_of_students[position].student_name;
            cout << "\n";

            cout << "Enter student gender:";
            cin >> array_of_students[position].student_gender;
            cout << "\n";

            cout << "Enter student's quizz one result:";
            cin >> array_of_students[position].quizz_one;
            cout << "\n";

            cout << "Enter student's quizz two result:";
            cin >> array_of_students[position].quizz_two;
            cout << "\n";

            cout << "Enter student's mid-term result: ";
            cin >> array_of_students[position].mid_term_score;
            cout << "\n";

            cout << "Enter student's second term result: ";
            cin >> array_of_students[position].final_score;
            cout << "\n";

            cout << "\n" << "STUDENT IS SUCCESFULLY ADDED! " << endl;
            position++;
        }
    }
    cout << "***************************************************" << endl;
    cout << "\n";
}

void update_student(student array_of_students[], int size_of_stArray)
{
    cout << "******** UPDATING INFORMATIONS ABOUT STUDENT ********" << endl;
    int position;
    cout << "Enter position of a student record: " << endl;
    cin >> position;

    for (int i = 0; i < size_of_stArray; i++)
    {
        if(position == i)
        {
            cout << "THIS IS A LIST OF INFORMATIONS YOU CAN CHANGE: " << endl;
            cout << "(1) Student ID number " << endl;
            cout << "(2) Student name " << endl;
            cout << "(3) Student gender " << endl;
            cout << "(4) Student quizz one result " << endl;
            cout << "(5) Student quizz two result " << endl;
            cout << "(6) Student mid-term result " << endl;
            cout << "(7) Student 2nd-term result" << endl;


            int input;
            cout << "\n" << "Please choose an option: ";
            cin >> input;

            while (input < 1 || input > 7)
            {
                cout << "You did not enter a correct choice!!" << endl;
                cout << "Please chooose a correct option: ";
                cin >> input;
            }

            switch(input)
            {
                case 1:
                    {
                        cout << "Student ID number at the moment is: " << array_of_students[i].ID_student << "\n" << endl;
                        int new_ID;
                        cout << "Enter new student ID number: ";
                        cin >> new_ID;

                        array_of_students[i].ID_student = new_ID;

                        cout << "Student ID has changed to: " << array_of_students[i].ID_student << endl;
                        break;

                    }
                case 2:
                    {
                        cout << "Student name at the moment is: " << array_of_students[i].student_name << "\n" << endl;
                        string new_name;
                        cout << "Enter new student name: ";
                        cin >> new_name;

                        array_of_students[i].student_name = new_name;

                        cout << "Student name has changed to: " << array_of_students[i].student_name << endl;
                        break;

                    }
                case 3:
                    {
                        cout << "Student gender at the moment is: " << array_of_students[i].student_gender << "\n" << endl;
                        char new_gender;
                        cout << "Enter new student gender: ";
                        cin >> new_gender;

                        array_of_students[i].student_gender = new_gender;

                        cout << "Student gender has changed to: " << array_of_students[i].student_gender << endl;
                        break;

                    }
                case 4:
                    {
                        cout << "Student's quizz one result at the moment is: " << array_of_students[i].quizz_one << "\n" << endl;
                        int new_quizz1;
                        cout << "Enter new student quizz one result: ";
                        cin >> new_quizz1;

                        array_of_students[i].quizz_one = new_quizz1;

                        cout << "Student's quizz one result has changed to: " << array_of_students[i].quizz_one << endl;
                        break;

                    }
                case 5:
                    {
                        cout << "Student's quizz two result at the moment is: " << array_of_students[i].quizz_two << "\n" << endl;
                        int new_quizz2;
                        cout << "Enter new student quizz two result: ";
                        cin >> new_quizz2;

                        array_of_students[i].quizz_two = new_quizz2;

                        cout << "Student's quizz two result has changed to: " << array_of_students[i].quizz_two << endl;
                        break;

                    }
                case 6:
                    {
                        cout << "Student's mid-term result at the moment is: " << array_of_students[i].mid_term_score << "\n" << endl;
                        int new_mid_term;
                        cout << "Enter new student mid-term result: ";
                        cin >> new_mid_term;

                        array_of_students[i].mid_term_score = new_mid_term;

                        cout << "Student's mid-term result has changed to: " << array_of_students[i].mid_term_score << endl;
                        break;
                    }
                case 7:
                    {
                        cout << "Student's 2nd-term result at the moment is: " << array_of_students[i].final_score << "\n" << endl;
                        int new_final;
                        cout << "Enter new student 2nd-term result: ";
                        cin >> new_final;

                        array_of_students[i].final_score = new_final;

                        cout << "Student's 2nd-term result has changed to: " << array_of_students[i].final_score << endl;
                        break;
                    }

                default: cout << "You did not enter a correct choice!!" << endl;
                break;
            }
        }
    }
    cout << "*****************************************************" << endl;
    cout << "\n";
}
void delete_student(student array_of_students[], int size_of_stArray)
{
    cout << "******** DELETING STUDENT: ********" << "\n" << endl;

    cout << "List of existing student ID numbers: " << endl;
    for(int i = 0; i < size_of_stArray; i++)
    {
        cout << "Student no. " << i+1 << " ID number:" << array_of_students[i].ID_student << "\n" << endl;
    }

    int student_id = 0;

    cout << "Enter ID of a student which you want delete: ";
    cin >> student_id;

    cout << "\n" << "List of student after deleting look like: " << endl;

    for(int i = 0; i < size_of_stArray; i++)
    {
        if (student_id == array_of_students[i].ID_student)
        {
            array_of_students[i].ID_student = ' ';
            array_of_students[i].student_name = ' ' ;
            array_of_students[i].student_gender = ' ';
            array_of_students[i].quizz_one = ' ';
            array_of_students[i].quizz_two = ' ';
            array_of_students[i].mid_term_score = ' ';
            array_of_students[i].final_score = ' ';

            cout << "Podaci o ovom studentu su izbrisani" << endl;
        }
        else
        {
            cout << "Student no. " << i+1 << endl;
            cout << "Student ID number: " << array_of_students[i].ID_student << endl;
            cout << "Student name: " << array_of_students[i].student_name << endl;
        }
    }
    cout << "**************************************************" << endl;
    cout << "\n";

}

void print_all_students(student array_of_students[], int size_of_stArray)
{
    cout << "******** PRINTING STUDENT INFO ********" << endl;
    for(int i = 0; i < size_of_stArray; i++)
    {
        cout << "STUDENT NO. "<< i+1 << endl;
        cout << "\n";

        cout << "student ID number: " << array_of_students[i].ID_student << endl;
        cout << "student name: " << array_of_students[i].student_name << endl;
        cout << "student gender: " << array_of_students[i].student_gender << endl;
        cout << "student quizz one result: " << array_of_students[i].quizz_one << endl;
        cout << "student quizz two result: " << array_of_students[i].quizz_two << endl;
        cout << "student mid-term result: " << array_of_students[i].mid_term_score << endl;
        cout << "student 2nd-term result: " << array_of_students[i].final_score << endl;
        cout << "student final grade: " << array_of_students[i].year_final_scores << endl;

        cout << "\n";
    }
    cout << "************************************************" << endl;
    cout << "\n";
}

void calculate_year_final(student array_of_students[],int size_of_stArray, int quizz_one,int quizz_two, int mid_term_score, int final_score)
{
    for(int i = 0; i < size_of_stArray; i++)
    {
        int scores = (array_of_students[i].quizz_one + array_of_students[i].quizz_two + array_of_students[i].mid_term_score + array_of_students[i].final_score)/4;
        array_of_students[i].year_final_scores = scores;
        /*if(scores > 0 && scores < 50)
        {
            array_of_students[i].year_final_scores = 1;
        }
        else if (scores >= 50 && scores <= 60)
        {
            array_of_students[i].year_final_scores = 2;
        }
        else if(scores > 60 && scores <= 75)
        {
            array_of_students[i].year_final_scores = 3;
        }
        else if(scores > 75 && scores <= 88)
        {
            array_of_students[i].year_final_scores = 4;
        }
        else if(scores > 88 && scores <= 100)
        {
            array_of_students[i].year_final_scores = 5;
        }
        else
        {
            cout << "Error! Check previous results!!" << endl;
        }*/
        cout << "Student: " << array_of_students[i].student_name << " ," << "year final score: " << array_of_students[i].year_final_scores << endl;
    }

}

void search_for_best_student(student array_of_students[], int size_of_stArray)
{
    int best_result = array_of_students[0].year_final_scores, i;
    for (i = 1; i < size_of_stArray; i++)
    {
        if(best_result < array_of_students[i].year_final_scores)
        {
            best_result = array_of_students[i].year_final_scores;
            cout <<"\n" << "NAJBOLJ/A STUDENT/ICA JE: " << array_of_students[i].student_name << " S REZULTATOM OD: " << best_result << " BODA/BODOVA." << endl;
        }
    }
}

void search_worst_student(student array_of_students[], int size_of_stArray)
{
    int worst_result = array_of_students[0].year_final_scores, i;
    for (i = 0; i < size_of_stArray; i++)
    {
        if(worst_result > array_of_students[i].year_final_scores)
        {
            worst_result = array_of_students[i].year_final_scores;
            cout << "\n" << "NAJLOSIJI/A STUDENT/ICA JE: " << array_of_students[i].student_name << " S REZULTATOM OD: " << worst_result << " BODA/BODOVA." << endl;
        }
    }
}

void search_using_studentID(student array_of_students[], int size_of_stArray)
{
    cout << "******** SEARCH FOR SPECIFIC STUDENT USING ID NUMBER ********" << endl;

    int student_id;
    cout << "\n" << "Enter student ID: " << endl;
    cin >> student_id;

    for(int i = 0; i < size_of_stArray; i++)
    {
        if(student_id == array_of_students[i].ID_student)
        {
            cout << "Student You searched for is: " << array_of_students[i].student_name << endl;
        }
    }
}

void sort_students(student array_of_students[], int size_of_stArray)
{
    int i, j;
    student temp[20];
    for(i = 0; i < size_of_stArray; i++)
    {
        for(j = 1; j < size_of_stArray; j++)
        {
            if(array_of_students[i].year_final_scores > array_of_students[j].year_final_scores)
            {
                temp[i]=array_of_students[i];
                array_of_students[i]=array_of_students[j];
                array_of_students[j]=temp[j];
            }
        }
    }
    cout << "The array after sorting is :\n" << endl;
    for(i = 0; i < size_of_stArray; i++)
    {
        cout << array_of_students[i].student_name << " " << array_of_students[i].year_final_scores << endl;
    }
}

void displayMenu()
{
    cout << "WELCOME TO STUDENT EVIDENCE AND TRACKING ANALYSE PROGRAM" <<endl;
    cout << "~~~ powered by @OOP lab 2019 ~~~" << "\n" << endl;

    cout << "THIS IS A LIST OF OPTIONS YOU CAN DO: " << endl;
    cout << "(1) Add student record " << endl;
    cout << "(2) Update student record " << endl;
    cout << "(3) Delete student record " << endl;
    cout << "(4) Search for the best student " << endl;
    cout << "(5) Search for the worst student " << endl;
    cout << "(6) Search for specific student record using student ID number " << endl;
    cout << "(7) Sort students by year final score " << endl;
    cout << "(8) Display all student records " << endl;
    cout << "(9) Exit from program " << endl;
}


int main()
{
    int num_of_st = 0;
    cout << "Firstly, let us know how big is your class of students: ";
    cin >> num_of_st;
    cout << "\n";

    student array_of_students[num_of_st] = {0};
    int size_of_stArray = num_of_st;
    int i, position=0, mid_term_score, final_score, year_final_score, quizz_one, quizz_two;

    bool exitMenu = false;
    while(!exitMenu){
        displayMenu();


        int input;
        cout << "\n" << "Please choose an option: ";
        cin >> input;
        cout << "\n";

        while (input < 1 || input > 9)
        {
            cout << "You did not enter a correct choice!!" << endl;
            cout << "Please chooose a correct option: ";
            cin >> input;
        }

        switch(input)
        {
            case 1:
                {
                    add_student(array_of_students, size_of_stArray, position);
                    calculate_year_final(array_of_students, size_of_stArray, quizz_one, quizz_two, mid_term_score, final_score);
                    break;
                }
            case 2:
                {
                    update_student(array_of_students, size_of_stArray);
                    break;
                }
            case 3:
                {
                    delete_student(array_of_students, size_of_stArray);
                    break;
                }
            case 4:
                {
                    search_for_best_student(array_of_students, size_of_stArray);
                    break;
                }
            case 5:
                {
                    search_worst_student(array_of_students, size_of_stArray);
                    break;
                }
            case 6:
                {
                    search_using_studentID(array_of_students, size_of_stArray);
                    break;
                }
            case 7:
                {
                    sort_students(array_of_students, size_of_stArray);
                    break;
                }
            case 8:
                {
                    print_all_students(array_of_students, size_of_stArray);
                    break;
                }

            case 9:
                {
                    exitMenu = true;
                    break;
                }

            default:
                {
                    cout << "You did not enter a correct choice!!" << endl;
                    break;
                }
        }
    }
}
