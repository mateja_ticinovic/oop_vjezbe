#include <iostream>

using namespace std;

int& getting_digits(int numb_array[], int size_of_numb_array, int& x)
{
    int hundreds = 0, units = 0, i;
    for(i = 0; i < size_of_numb_array; i++)
    {
        //cout << "broj je: " << numb_array[i] << "\n" << endl;
        int sum = 0;
        hundreds = numb_array[i] / 100 % 10;
        units = numb_array[i] % 10;
        sum = hundreds + units;
        //cout << "stotica je: " << hundreds << ", a jedinica: " << units << "a zbroj je: " << sum << "\n" << endl;

        if (sum == 5)
        {
             x = numb_array[i];
             //cout << x << endl;
        }

    }
    return x;

}

int main()
{
    int numb_array[] = {1235,1365,1124,5555,6666,7898};
    int size_of_numb_array = sizeof (numb_array)/ sizeof (numb_array[0]),x;

    ++getting_digits(numb_array, size_of_numb_array,x);

    cout << "Trazeni broj uvecan za 1 iznosi: " << x << endl;
}
