#ifndef TARGET_H
#define TARGET_H
#include <vector>
#include "point.h"

class Target
{
    public:
        Point bottom_left_point;
        double target_width;
        double target_height;

        bool shooted_or_not;

        Target setting_values(Target t, Point bottom_left_point, double target_width, double target_height);
        void display_target_values(Target t, vector<Target>& targets);
        double setting_target_p_z_value(Target t, vector<Target>& targets, int pos);
        double setting_target_k_z_value(Target t, vector<Target>& targets, int pos);

};

#endif // TARGET_H
