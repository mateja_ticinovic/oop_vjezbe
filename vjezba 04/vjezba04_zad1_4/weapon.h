#ifndef WEAPON_H
#define WEAPON_H
#include "point.h"


class Weapon
{
    public:
        Point position;
        int total_bullets;
        int current_bullets;

    public:
        Weapon setting_values(Weapon w, Point position, int total_bullets, int current_bullets);
        void weapon_shoot(int total_bullets, int current_bullets);
        void weapon_reload(int total_bullets, int current_bullets);
        void display_value(Weapon w);
        double getting_weapon_z_value(Weapon w);
};

#endif // WEAPON_H
