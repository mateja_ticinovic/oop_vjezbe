#include "target.h"


Target Target::setting_values(Target t, Point bottom_left_point, double target_width, double target_height)
{
    t.bottom_left_point = bottom_left_point;
    t.target_width = target_width;
    t.target_height = target_height;

    t.shooted_or_not = false;

    return t;
}

void Target::display_target_values(Target t, vector<Target>& targets)
{
    for(int i = 0; i < targets.size(); i++)
    {
        cout << "DISPLAYING TARGET VALUES: " << endl;
        cout << "Target position is: ";
        cout << "lenght: " << targets[i].bottom_left_point.lenght << ", width: " << targets[i].bottom_left_point.width << ", height: " << targets[i].bottom_left_point.height << endl;
        cout << "Target heigth is: " << targets[i].target_height << endl;
        cout << "Target width is: " << targets[i].target_width << endl;

        if(targets[i].shooted_or_not == false)
        {
            cout << "Target is missed." << endl;
        }
        else
        {
            cout << "Target is shooted." << endl;
        }
    }
}

double Target::setting_target_p_z_value(Target t, vector<Target>& targets, int pos)
{
    return targets[pos].bottom_left_point.height;
}

double Target::setting_target_k_z_value(Target t, vector<Target>& targets, int pos)
{
    return targets[pos].bottom_left_point.height + 5;
}
