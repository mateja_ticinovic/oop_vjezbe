#include "weapon.h"


Weapon Weapon::setting_values(Weapon w, Point position, int total_bullets, int current_bullets)
{
    w.position = position;

    w.total_bullets = total_bullets;
    w.current_bullets = current_bullets;

    return w;
}

void Weapon::display_value(Weapon w)
{
    cout << "Weapon's position is: " << endl;
    cout << "lenght: " << w.position.lenght << ", width: " << w.position.width << ", height: "<< w.position.height << endl;
    cout << "Weapon's total bullets number is: " << " " << w.total_bullets << endl;
    cout << "Weapon's current bullets number is : " << " " << w.current_bullets << endl;
}

void Weapon::weapon_shoot(int total_bullets, int current_bullets)
{
    current_bullets = total_bullets;
    current_bullets--;
    cout << "After shooting remained " << current_bullets << " bullets in weapon." << endl;
}

 void Weapon::weapon_reload(int total_bullets, int current_bullets)
 {
    current_bullets = total_bullets;
 }

 double Weapon::getting_weapon_z_value(Weapon w)
 {
     double z;

     z = w.position.height;
     return z;
 }
