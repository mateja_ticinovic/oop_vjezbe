#include <iostream>
#include <time.h>
#include <algorithm>
#include <vector>
#include "point.h"
#include "weapon.h"
#include "target.h"

using namespace std;

int main()
{
    srand(time(NULL));
    Point p1,other;

    double low_border, max_border;

    double lenght = 0, width = 0, height = 0;

    /*cout << "ENTERING POINT'S VALUES: " << endl;
    cout << "Enter your point's lenght:";
    cin >> lenght;

    cout << "Enter your point's width:";
    cin >> width;

    cout << "Enter your point's height:";
    cin >> height;

    p1 = p1.setting_values(p1, lenght, width, height);
    other = other.setting_random_values(other, low_border, max_border);

    cout << "*******************************************" << endl;
    cout << "DISPLAYING HANDLY INPUTTED POINT'S VALUES:" << endl;
    p1.display_value(p1);
    cout << "*******************************************" << endl;

    cout << "DISPLAYING RANDOMLY INPUTTED POINT'S VALUES:"  << endl;
    other.display_value(other);
    cout << "*******************************************" << endl;

    double result_2d, result_3d;

    result_2d = p1.count_distance_2d(p1, other);
    cout << "Distance between 2D points is: " << " " << result_2d << endl;
    cout << "*******************************************" << endl;

    result_3d = p1.count_distance_3d(p1, other);
    cout << "Distance between 3D points is: " << " " << result_3d << endl;
    cout << "-------------------------------------------" << endl;*/

    Weapon w;

    double pos_lenght = 0, pos_width = 0, pos_height = 0, w_height = 0;
    int total_bullets, current_bullets;
    Point position;

    cout << "ENTERING WEAPON VALUES: " << endl;
    cout << "Enter your point's lenght(weapon): ";
    cin >> pos_lenght;
    position.lenght = pos_lenght;

    cout << "Enter your point's width(weapon): ";
    cin >> pos_width;
    position.width = pos_width;

    cout << "Enter your point's height(weapon) : ";
    cin >> pos_height;
    position.height = pos_height;

    cout << "Enter total bullets for your weapon: ";
    cin >> total_bullets;

    cout << "Enter current bullets number: ";
    cin >> current_bullets;
    cout << "*******************************************" << endl;

    w = w.setting_values(w, position, total_bullets, current_bullets);

    cout << "DISPLAYING WEAPON VALUES: " << endl;
    w.display_value(w);
    cout << "*******************************************" << endl;

    cout << "GETTING WEAPON HEIGHT: " << endl;
    w_height = w.getting_weapon_z_value(w);
    cout << "Weapon height(Z) is: " << w_height << "." << endl;
    cout << "*******************************************" << endl;

    w.weapon_shoot(total_bullets, current_bullets);
    cout << "*******************************************" << endl;

    if(current_bullets == 0)
        w.weapon_reload(total_bullets, current_bullets);

    cout << "-------------------------------------------" << endl;

    Target t;
    vector<Target> targets;
    Point bottom_left_point;
    double target_height = 0, target_width = 0;
    int number_of_targets;
    cout << "Enter number of targets you want to have: " << endl;
    cin >> number_of_targets;

    cout << "ENTERING TARGET VALUES: " << endl;

    for(int i = 0; i < number_of_targets; i++)
    {
        cout << "POSITION OF TARGET: " << endl;
        cout << "Enter position lenght: ";
        cin >> bottom_left_point.lenght;

        cout << "Enter position width: ";
        cin >> bottom_left_point.width;

        cout << "Enter position height: ";
        cin >> bottom_left_point.height;

        cout << "TARGET WIDTH: " << endl;
        cout << "Enter target's width: ";
        cin >> target_width;

        cout << "TARGET HEIGHT: " << endl;
        cout << "Enter target's height: ";
        cin >> target_height;

        t.shooted_or_not = false;
        t = t.setting_values(t, bottom_left_point, target_width, target_height);
        targets.push_back(t);
    }
    t.display_target_values(t, targets);
    cout << "*******************************************" << endl;

    /*double targ_p_z = t.setting_target_p_z_value(t, targets, 1);
    cout << "Target p_z value is: " << targ_p_z << endl;

    double targ_k_z = t.setting_target_k_z_value(t, targets, 1);
    cout << "Target k_z value is: " << targ_k_z << endl;*/

    for(int x = 0; x < targets.size(); x++)
    {
        if((w_height > targets[x].bottom_left_point.height) && (w_height < targets[x].bottom_left_point.height + targets[x].target_height))
        {
            targets[x].shooted_or_not = true;
        }
        else
        {
            x++;
        }
    }
    t.display_target_values(t,targets);
}
