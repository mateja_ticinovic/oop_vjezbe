#ifndef POINT_H
#define POINT_H
#include <math.h>
#include <iostream>
#include <algorithm>

using namespace std;

class Point
{
    public:
        double lenght = 0;
        double width = 0;
        double height = 0;

    public:
        Point setting_values(Point p, double lenght, double width, double height);
        Point setting_random_values(Point p, double a, double b);
        void display_value(Point p);
        double count_distance_2d(Point p1, Point p2);
        double count_distance_3d(Point p1, Point p2);

};

#endif // POINT_H
