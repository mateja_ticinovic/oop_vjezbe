#include "point.h"
#include <time.h>

Point Point::setting_values(Point p, double lenght, double width, double height)
{
    p.lenght = lenght;
    p.width = width;
    p.height = height;

    return p;
}

Point Point::setting_random_values(Point p, double a, double b)
{
    cout << "\n";

    cout << "Enter low border: ";
    cin >> a;

    cout << "Enter max border: ";
    cin >> b;


    double no1, no2, no3;

    double f = (double)rand() / RAND_MAX;
    no1 = a + f * (b - a);
    p.lenght = no1;

    double f1 = (double)rand() / RAND_MAX;
    no2 = a + f1 * (b - a);
    p.width = no2;

    double f2 = (double)rand() / RAND_MAX;
    no3 =  a + f2 * (b - a);
    p.height = no3;

    return p;
}

void Point::display_value(Point p)
{
    cout << "Point's lenght is: " << " " << p.lenght << endl;
    cout << "Point's width is: " << " " << p.width << endl;
    cout << "Point's height is: " << " " << p.height << endl;
}

double Point::count_distance_2d(Point p1, Point p2)
{
    double result = 0;

    double x = p1.lenght;
    double y = p1.width;

    double a = p2.lenght;
    double b = p2.width;

    result = sqrt(pow(x - a, 2.0) + pow(y - b, 2.0));

    return result;

}

double Point::count_distance_3d(Point p1, Point p2)
{
    double result = 0;

    double x1 = p1.lenght;
    double y1 = p1.width;
    double z1 = p1.height;

    double x2 = p2.lenght;
    double y2 = p2.width;
    double z2 = p2.height;

    result = sqrt(pow(x1-x2, 2.0) + pow(y1-y2, 2.0) + pow(z1-z2, 2.0));

    return result;
}
