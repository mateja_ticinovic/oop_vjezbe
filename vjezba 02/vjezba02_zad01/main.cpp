#include <iostream>

using namespace std;

void input_numbers(int array_of_numbers[], int len)
{
    int number;
    for(int i = 0; i < len; i++)
    {
        cout << "Enter number 1-9: ";
        cin >> number;

        array_of_numbers[i] = number;
    }
}

int find_missing_number(int array_of_numbers[], int lenght)
{
    int i, total;

    total = (lenght + 1) * (lenght + 2) / 2;

    for ( i = 0; i< lenght; i++)
        total -= array_of_numbers[i];

    array_of_numbers[lenght] = total;

    return total;

}

void sorting_array(int array_of_numbers[], int lenght)
{
    for(int i = 0; i < lenght + 1; i++)
    {
        for(int j = 0; j < (lenght + 1 - i); j++)
        {
            if(array_of_numbers[j] > array_of_numbers[j+1])
            {
                int temp = array_of_numbers[j];
                array_of_numbers[j] = array_of_numbers[j+1];
                array_of_numbers[j+1] = temp;

            }
        }
    }
    for(int i = 0; i < lenght+1;i++)
    {
        cout << array_of_numbers[i] << endl;
    }
}

int main()
{
    int lenght = 0;
    cout << "Enter array lenght: ";
    cin >> lenght;

    int* array_of_numbers;
    array_of_numbers = new int[lenght];

    int number = 0, i, num_sum;

    cout << "\n";

    input_numbers(array_of_numbers, lenght);

    int missingNo = find_missing_number(array_of_numbers, lenght);
    cout << "MISSING NUMBER IS: " << missingNo << endl;

    sorting_array(array_of_numbers, lenght);

    cout << "FINALLY, ARRAY LOOKS LIKE THIS:";
    for(int j = 0; j < lenght + 1; j++)
    {
        cout << array_of_numbers[j] << " ";
    }

    delete [] array_of_numbers;
    array_of_numbers = 0;
}
