#include <iostream>

using namespace std;

void separating_numbers(int array_of_numbers[], int lenght)
{
    int i = 0, j = lenght-1;

    while(i < j)
    {
        while(array_of_numbers[i] % 2 == 0 && i < j)
            i++;
        while(array_of_numbers[j] % 2 == 1 && i < j)
            j--;

        if(i < j)
        {
            int temp = array_of_numbers[i];
            array_of_numbers[i] = array_of_numbers[j];
            array_of_numbers[j] = temp;
            i++;
            j--;
        }
    }
}
void printing_array(int array_of_numbers[], int lenght)
{
    for(int j = 0; j < lenght; j++)
    {
        cout << array_of_numbers[j] << " ";
    }
}
int main()
{
    int lenght = 0;

    cout << "Enter array's lenght: ";
    cin >> lenght;

    int* array_of_numbers;
    array_of_numbers = new int[lenght];

    int number = 0, i;

    cout << "\n";
    for(i = 0; i < lenght; i++)
    {
        cout << "Enter number: ";
        cin >> number;

        array_of_numbers[i] = number;
    }
    cout << "\n";

    cout << "BEFORE SEPARATING ARRAY LOOKS LIKE: ";
    printing_array(array_of_numbers, lenght);

    cout << "\n";
    separating_numbers(array_of_numbers, lenght);
    cout << "AFTER SEPARATING ARRAY LOOKS LIKE: ";
    printing_array(array_of_numbers,lenght);

}
