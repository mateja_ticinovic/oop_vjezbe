#include <iostream>

using namespace std;

struct vektor{
    int logicka_velicina;
    int fizicka_velicina;
    int* elementi;

    void vector_new(int fizicka)
    {
        logicka_velicina = 0;
        fizicka_velicina = fizicka;
        elementi = new int[fizicka];
    }

    void vector_delete()
    {
        delete[] elementi;
        delete this;
    }

    void vector_push_back(int broj)
    {
        if(logicka_velicina == fizicka_velicina)
        {
            fizicka_velicina = logicka_velicina*2;
            int* novi = new int [fizicka_velicina];
            copy(elementi, elementi + logicka_velicina, novi);
            delete []elementi;
            elementi = novi;
        }
        elementi[logicka_velicina] = broj;
        logicka_velicina++;
    }
    void vector_pop_back()
    {
        logicka_velicina --;
    }

    int vector_front()
    {
        return elementi[0];
    }

    int vector_back()
    {
       return elementi[logicka_velicina-1];
    }

    int vector_size()
    {
        return logicka_velicina;
    }

    };
int main()
{
    vektor v;
    v.vector_new(3);
    v.vector_push_back(12);
    v.vector_push_back(12);
    v.vector_push_back(1994);
    v.vector_push_back(24);

    v.vector_pop_back();
    for(int i = 0 ; i< v.vector_size(); i++)
        cout << v.elementi[i]<< endl;
    cout << v.vector_size();
}

