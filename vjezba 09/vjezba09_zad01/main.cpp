#include <iostream>
#include <vector>
#include <algorithm>
#include "pair.h"

int main()
{
    using namespace std;

    Pair<double, int> a(1);
    Pair<double, int> b(7,5);
    Pair<double, int> c;
    cout << a << "\n"<< b << "\n" << c <<"\n" ;

    cout << "veci " << (a>b) <<"\nmanji "<< (a<b) <<"\njednak "<< (a!=b) << endl;

    a.swapp(b);
    cout << "a " << a <<"\nb " << b << "\n";

    Pair<char*, char*> p1, p2, p3;
    vector<Pair<char*, char*> > v;

    cin >> p1 >> p2 >> p3;

    v.push_back(p1);
    v.push_back(p2);
    v.push_back(p3);

    sort(v.begin(), v.end());

    vector<Pair<char*, char*> >::iterator it;
    for (it = v.begin(); it != v.end(); ++it)
        cout << *it << endl;

}
