#ifndef PAIR_H
#define PAIR_H
#include <iostream>


template<typename t1, typename t2>
class Pair
{
     private:
        t1 first;
        t2 second;

    public:

        Pair(const t1& def = 5, const t2& def2 = 7): first(def), second(def2){};
        Pair(const Pair<t1, t2>& other): first(other.first), second(other.second){};
        ~Pair(){};

        bool operator== (const Pair<t1,t2>& other) const
        {
            return first == other.first && second == other.second;
        }

        bool operator!= (const Pair<t1,t2>& other) const
        {
            return !(first == other.first && second == other.second);
        }

        bool operator< (const Pair<t1,t2>& other) const
        {
            return first == other.first ? (second < other.second ? true : false) : (first < other.first ? true : false);
        }

         bool operator> (const Pair<t1,t2>& other) const
        {
            return first == other.first ? (second > other.second ? true : false) : (first > other.first ? true : false);
        }

        bool operator<= (const Pair<t1,t2>& other) const
        {
            return *this == other ? true : *this < other ? true : false;
        }

        bool operator>= (const Pair<t1, t2>& other) const
        {
            return *this == other ? true : *this > other ? true : false;
        }

        void operator= (const Pair<t1, t2>& other) const
        {
            first = other.first;
            second = other.second;
        }

        friend std::ostream& operator<< (std::ostream& os, const Pair<t1, t2>& t)
        {
            os << t.first << " " << t.second;
            return os;
        }

        friend std::istream& operator>> (std::istream& is, const Pair<t1,t2>& t)
        {
            is >> t.first >> t.second;
            return is;
        }

        Pair<t1, t2> swapp (Pair<t1,t2>& other)
        {
            t1 temp = first;
            t2 temp2 = second;

            first = other.first;
            second = other.second;

            other.first = temp;
            other.second = temp2;
        }
};

template <>
class Pair<char*, char*>
{
    private:
        char first;
        char second;

    public:
        Pair(const char& def = 'z', const char& def2 = 'z'): first(def), second(def2){};
        Pair(const Pair<char*, char*>& other): first(other.first), second(other.second){};
        ~Pair(){};

        friend std::ostream& operator<< (std::ostream& os, const Pair<char*, char*>& c)
        {
            os << c.first << " " << c.second;
            return os;
        }

        friend std::istream& operator>>(std::istream& is,  Pair<char*, char*>& c)
        {
            is >> c.first >> c.second;
            return is;
        }

        bool operator< (const Pair<char*,char*>& other) const
        {
            return first == other.first ? (second < other.second ? true : false) : (first < other.first ? true : false);
        }

};


#endif // PAIR_H

